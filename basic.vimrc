set nocompatible
" plugin indent guide
let g:indent_guides_enable_on_vim_startup = 1
" plugin aling
let g:Align_xstrlen = 3

" CTRL-hjklでウィンドウ移動
nnoremap wj <C-w>j
nnoremap wk <C-w>k
nnoremap wl <C-w>l
nnoremap wh <C-w>h

nnoremap <silent><C-e> :NERDTreeToggle<CR>
	nmap <silent> <C-e>      :NERDTreeToggle<CR>
	vmap <silent> <C-e> <Esc>:NERDTreeToggle<CR>
	omap <silent> <C-e>      :NERDTreeToggle<CR>
	imap <silent> <C-e> <Esc>:NERDTreeToggle<CR>
	cmap <silent> <C-e> <C-u>:NERDTreeToggle<CR>
let g:NERDTreeIgnore=['\.clean$', '\.swp$', '\.bak$', '\~$']
let g:NERDTreeShowHidden=1
let g:NERDTreeDirArrows=0
let g:NERDTreeMinimalUI=1
let g:NERDTreeMouseMode=2

" .vimrcのエンコーディング
scriptencoding utf-8
" ファイルフォーマット(LF)
set fileformat=unix
" 文字コード自動認識
set fileencodings=iso-2022-jp,utf-8,cp932,euc-jp,default,latin

" backup
set nowritebackup
set nobackup
set noswapfile

" クリップボードを共有
"set clipboard+=unnamed
" 8進数を無効にする。<C-a>,<C-x>に影響する
set nrformats-=octal
" キーコードやマッピングされたキー列が完了するのを待つ時間(ミリ秒)
set timeout timeoutlen=3000 ttimeoutlen=100
" 編集結果非保存のバッファから、新しいバッファを開くときに警告を出さない
set hidden
" ヒストリの保存数
set history=50
" 日本語の行の連結時には空白を入力しない
set formatoptions+=mM
" Visual blockモードでフリーカーソルを有効にする
set virtualedit=block
" カーソルキーで行末／行頭の移動可能に設定
set whichwrap=b,s,[,],<,>
" バックスペースでインデントや改行を削除できるようにする
set backspace=indent,eol,start

" □や○の文字があってもカーソル位置がずれないようにする
set ambiwidth=double
" コマンドライン補完するときに強化されたものを使う
set wildmenu

" マウスを有効にする
"if has('mouse')
"  set mouse=a
"endif
" pluginを使用可能にする
filetype plugin indent on

"----------------------------------------
" 検索
"----------------------------------------
" 検索の時に大文字小文字を区別しない
" ただし大文字小文字の両方が含まれている場合は大文字小文字を区別する
set ignorecase
set smartcase
" 検索時にファイルの最後まで行ったら最初に戻る
set wrapscan
" インクリメンタルサーチ
set incsearch
" 検索文字の強調表示
set hlsearch
" w,bの移動で認識する文字
" set iskeyword=a-z,A-Z,48-57,_,.,-,>
" vimgrep をデフォルトのgrepとする場合internal
" set grepprg=internal

"----------------------------------------
" 表示設定
"----------------------------------------
" スプラッシュ(起動時のメッセージ)を表示しない
" set shortmess+=I
" エラー時の音とビジュアルベルの抑制(gvimは.gvimrcで設定)
set noerrorbells
set novisualbell
set visualbell t_vb=
" マクロ実行中などの画面再描画を行わない
" set lazyredraw
" Windowsでディレクトリパスの区切り文字表示に / を使えるようにする
set shellslash
" 行番号表示
set number
" 括弧の対応表示時間
set showmatch matchtime=1
" 自動的にインデントする
set autoindent
" Cインデントの設定
set cinoptions+=:0
" タイトルを表示
set title
" コマンドラインの高さ (gvimはgvimrcで指定)
set cmdheight=2
set laststatus=2
" コマンドをステータス行に表示
"set showcmd
" 画面最後の行をできる限り表示する
set display=lastline
" Tab、行末の半角スペースを明示的に表示する
set list
set listchars=tab:>-

" 外部のエディタで編集中のファイルが変更されたら、自動的に読み直す
set autoread
" タブを表示する
set expandtab
" フォント
"setlocal encoding=cp932
"set guifont=あずきフォント:h14
"setlocal encoding=utf-8
" tabspace, 自動インデントの各段階に使われる空白の数,
" tabstopを変えずに空白を含めることにより、見た目のtabstopを変える
set ts=4 sw=4 sts=4
" カーソルが何行目の何列目に置かれているかを表示する
set ruler
" ウィンドウの幅を超える行の折り返し設定
set nowrap
" カーソルの上または下に表示する最小限の行数
set scrolloff=5
" コマンドをステータスラインに表示する
set showcmd
" 現在のモードを表示する
set showmode
" ウィンドウのタイトルを変更する設定
set title
" ターミナルで使われるエンコーディング名
set termencoding=utf-8
" ウィンドウの高さを行単位で指定する
"set lines=60
" スクリーン上の列幅
"set columns=300
" スワップファイルを使用する設定
set noswapfile
" 強調表示(色付け)のON/OFF設定
syntax on

""""""""""""""""""""""""""""""
" ステータスラインに文字コード等表示
" iconvが使用可能の場合、カーソル上の文字コードをエンコードに応じた表示にするFencB()を使用
""""""""""""""""""""""""""""""
"if has('iconv')
"	set statusline=%<%f\ %m\ %r%h%w%{'['.(&fenc!=''?&fenc:&enc).']['.&ff.']'}%=[0x%{FencB()}]\ (%v,%l)/%L%8P\
"else
"	set statusline=%<%f\ %m\
"	%r%h%w%{'['.(&fenc!=''?&fenc:&enc).']['.&ff.']'}%=\ (%v,%l)/%L%8P\
"endif

" FencB() : カーソル上の文字コードをエンコードに応じた表示にする
function! FencB()
    let c = matchstr(getline('.'), '.', col('.') - 1)
    let c = iconv(c, &enc, &fenc)
    return s:Byte2hex(s:Str2byte(c))
endfunction

function! s:Str2byte(str)
    return map(range(len(a:str)), 'char2nr(a:str[v:val])')
endfunction

function! s:Byte2hex(bytes)
    return join(map(copy(a:bytes), 'printf("%02X", v:val)'), '')
endfunction

"タブ、空白、改行の可視化
set list
set listchars=tab:>.,trail:_,extends:>,precedes:<

"全角スペースをハイライト表示
function! ZenkakuSpace()
    highlight ZenkakuSpace cterm=reverse ctermfg=DarkMagenta gui=reverse guifg=DarkMagenta
    highlight Visual ctermbg=238
endfunction

if has('syntax')
    augroup ZenkakuSpace
        autocmd!
        autocmd ColorScheme       * call ZenkakuSpace()
        autocmd VimEnter,WinEnter * match ZenkakuSpace /　/
    augroup END
    call ZenkakuSpace()
endif


function! s:get_syn_id(transparent)
    let synid = synID(line("."), col("."), 1)
    if a:transparent
        return synIDtrans(synid)
    else
        return synid
    endif
endfunction
function! s:get_syn_attr(synid)
    let name = synIDattr(a:synid, "name")
    let ctermfg = synIDattr(a:synid, "fg", "cterm")
    let ctermbg = synIDattr(a:synid, "bg", "cterm")
    let guifg = synIDattr(a:synid, "fg", "gui")
    let guibg = synIDattr(a:synid, "bg", "gui")
    return {
                \ "name": name,
                \ "ctermfg": ctermfg,
                \ "ctermbg": ctermbg,
                \ "guifg": guifg,
                \ "guibg": guibg}
endfunction
function! s:get_syn_info()
    let baseSyn = s:get_syn_attr(s:get_syn_id(0))
    echo "name: " . baseSyn.name .
                \ " ctermfg: " . baseSyn.ctermfg .
                \ " ctermbg: " . baseSyn.ctermbg .
                \ " guifg: " . baseSyn.guifg .
                \ " guibg: " . baseSyn.guibg
    let linkedSyn = s:get_syn_attr(s:get_syn_id(1))
    echo "link to"
    echo "name: " . linkedSyn.name .
                \ " ctermfg: " . linkedSyn.ctermfg .
                \ " ctermbg: " . linkedSyn.ctermbg .
                \ " guifg: " . linkedSyn.guifg .
                \ " guibg: " . linkedSyn.guibg
endfunction
command! SyntaxInfo call s:get_syn_info()    "   "

colorscheme molokai

syntax on

