set runtimepath^=$HOME/.vim

source ~/.vim/bundle.vimrc
source ~/.vim/unite.vimrc
source ~/.vim/basic.vimrc
